#!/bin/bash

set -e

. /etc/code-orange/netstack-uplink-failover/uplink-failover.conf

/sbin/ip route add ${check_ip_address} via ${main_gateway_ip}

if ping -c4 ${check_ip_address} ; then
        echo "DSL up"
        /sbin/ip route replace default via ${main_gateway_ip}
else
        echo "DSL down"
        /sbin/ip route replace default via ${failover_gateway_ip}
 fi

/sbin/ip route del ${check_ip_address}
